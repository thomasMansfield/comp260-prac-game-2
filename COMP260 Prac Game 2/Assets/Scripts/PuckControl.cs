﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    public LayerMask paddleLayer;
    public Transform startingPos;

    private AudioSource audio;
    private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        ResetPosition();
	}

    // Collision events
    void OnCollisionEnter(Collision collision)
    {
        //check what we've hit
        if (paddleLayer.Contains(collision.gameObject))
        {
            //hit the paddle
            audio.PlayOneShot(paddleCollideClip);
        } else
        {
            //hit something else
            audio.PlayOneShot(wallCollideClip);
        }
    }

    public void ResetPosition()
    {
        //teleport to starting position
        rigidbody.MovePosition(startingPos.position);
        //stop it from moving
        rigidbody.velocity = Vector3.zero;
    }
}
