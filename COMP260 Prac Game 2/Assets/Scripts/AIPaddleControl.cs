﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPaddleControl : MonoBehaviour {

    public float paddleSpeedMax = 4.0f;
    public float paddleSpeedMin = 6.0f;
    public float pushForce = 5.0f;
    public Rigidbody puckRigidbody;
    public LayerMask puckLayer;

    private Rigidbody rigidbody;  

	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
	}

	void FixedUpdate () {
        Vector3 dir = new Vector3();

        //determine where the puck is
        if (puckRigidbody.position.z > rigidbody.position.z + 0.5)
        {
            dir.z = 1;
        } else if (puckRigidbody.position.z < rigidbody.position.z - 0.5)
        {
            dir.z = -1;
        }

        Vector3 vel = dir.normalized * Random.Range(paddleSpeedMin, paddleSpeedMax);

        rigidbody.velocity = vel;
	}
}
