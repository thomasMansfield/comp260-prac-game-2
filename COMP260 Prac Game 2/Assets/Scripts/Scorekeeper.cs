﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {

    static private Scorekeeper instance;

    public int pointsPerGoal = 1;
    private int[] score = new int[2];

    public Text winText;
    public Text[] scoreText;

    static public Scorekeeper Instance
    {
        get { return instance; }
    }

    void Start () {
        if (instance == null)
        {
            //save this instance
            instance = this;
        } else
        {
            //throw an error, as there are two instances of this object
            Debug.LogError("More than one Scorekeeper exists in scene.");
        }
        
        //reset score to 0
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }

        //reset win text to nothing
        winText.text = "";
	}

    public void OnScoreGoal(int player)
    {
        score[player] += pointsPerGoal;
        scoreText[player].text = score[player].ToString();

        //check for win
        if (score[player] >= 10)
        {
            winText.text = "Player " + (player+1) + " wins!";

            StartCoroutine(winReset());
        }
    }

    IEnumerator winReset()
    {
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(3);
        Time.timeScale = 1;
        
        //reset everything
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }

        winText.text = "";
    }
}
